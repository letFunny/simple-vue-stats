import Vue from 'vue';
import App from './App.vue';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import store from './store';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSearch, faCheck, faCheckCircle, faInfoCircle,
  faExclamationTriangle, faExclamationCircle, faArrowUp, faAngleRight,
  faAngleLeft, faAngleDown, faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faSearch, faCheck, faCheckCircle, faInfoCircle,
  faExclamationTriangle, faExclamationCircle, faArrowUp, faAngleRight,
  faAngleLeft, faAngleDown, faEye, faEyeSlash, faCaretDown,
  faCaretUp, faUpload);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

new Vue({
  render: h => h(App),
  store,
}).$mount('#app');
