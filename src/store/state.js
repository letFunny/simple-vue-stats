import {data} from "@/data/countries.js"

// Default view contains the data for the film
const countries = data["Grand Budapest Hotel"];
const countriesOrdered = countries.sort((a, b) => {
    // b - a in order to put the highest first
    return b.Amount - a.Amount
});

export default {
    // Countries that are shown now.
    countries: countriesOrdered,
    // For prompt.
    isModalVisible: false,
    // Notification for status of the query.
    notification: false,
    // Query itself.
    query: "Grand Budapest Hotel",
    // All the local data to mimic a real server.
    data,
}