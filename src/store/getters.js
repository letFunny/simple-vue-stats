export default {
    // Gets the 10 countries with the highest numbers.
    getTop10Countries: (state) => {
        return state.countries.slice(0, 10);
    },
    // Creates the proper format from the bar chart.
    countriesToBar: (state, { getTop10Countries }) => {
        const countries = getTop10Countries.reverse();
        const labels = countries.map((country) => country.Country);
        const data = countries.map((country) => country.Amount);
        return {
            "labels": labels,
            "data": data
        }
    },
    // Given a query returns false if the query is not in the data and the data
    // itself if the query is correctly indexed.
    getQueryFromData: (state) => {
        return (query) => {
            if (query in state.data) {
                return state.data[query];
            } else {
                return false;
            }
        };
    }
};