export default {
    setCountries: (state, countries) => {
        state.countries = countries;
    },
    toggleModalComponent: (state) => {
        state.isModalVisible = !state.isModalVisible;
    },
    setNotification: (state, notification) => {
        state.notification = notification;
    },
    setQuery: (state, query) => {
        state.query = query;
    },
};