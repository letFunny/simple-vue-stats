# Introduction
This is a very simple data dashboard I was working on for a bigger project.
However, the project came to an end so I decided to polish my existing code
and publish it. It shows data for movie trends by using my other tool
[torrentstats](https://gitlab.com/letFunny/torrentstats) and a simple Python
scripts that transforms the data.

I have changed the source so that instead of querying a backend, it works with
static data so that I can publish it online (and not worry about torrents).

# Demo
I have uploaded this static website [here](https://simple-vue-stats.netlify.app/).
The data there is real and comes from the aforementioned tool. I have included
**Grand Budapest Hotel** based on a real torrent from a torrent site with a ship.
**Debian** is the official net iso and **This is not going to exist** is just
for trying the error codes.

If the website is down for some reason you can see some screenshots here:

![Main window](images/main.png)

![Search window](images/search.png)

# Run it locally
The code is standard Vue, JS and HTML. You just have to run yarn to collect
the dependencies and start the development server:
```sh
$ yarn serve
```
