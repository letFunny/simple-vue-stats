import axios from "axios";

export default {
  // Mimics the axios request to a server by querying the local data.
  fetchNewData: ({ commit, getters }, query) => {
    let res = getters.getQueryFromData(query);
    setTimeout(() => {
      if (res) {
        commit("setQuery", query);
        commit("setCountries", res);
        commit("setNotification", {
          message: "Data refreshed correctly",
          type: "is-success",
          duration: 3000,
        });
      }
      else {
        commit("setNotification", {
          message: "Data could not be fetched: Network Error",
          type: "is-danger",
          duration: 5000,
        });
      }
    }, 2000);
  },
  // Toggles the prompt modal.
  toggleModal: ({ commit }) => {
    commit("toggleModalComponent");
  },
  // Toggles the notification footer for data arrival/error.
  toggleNotification: ({ commit }, val) => {
    commit("setNotification", val);
  },
};
